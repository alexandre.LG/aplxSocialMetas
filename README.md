# aplxSocialMetas
Permet l'affichage des balises meta Open Graph, Schema.org et Twitter card.

## Ce que ça fait
- ajouter les balises Open Graph
- ajouter les balises Schema.org
- ajouter les balises Twitter Card

## Particularité
L'image est automatiquement récupérée sur les articles.
Une image par défaut peut être incluse si celle-ci est déposée dans le dossier de votre thème, puis "img" et nommée "aplxSocialMetas.png".

## Utilisation
Ajouter le code suivant dans "header.php" :
	<?php eval($plxShow->callHook('aplxSocialMetas')); ?>