<?php
	/*
	*	Plugin aplxmetas par Alexandre LG
	*	Permet l'affichage des balises pour les réseaux sociaux (OpenGraph, Twitter and co)
	*/

	class aplxSocialMetas extends plxPlugin {
		public function __construct($default_lang) {
			# appel du constructeur de la classe plxPlugin
			parent::__construct($default_lang);

			$this->addHook('aplxSocialMetas', 'showMetas'); // Style perso à CKEditor
		}
		
		/****
		* Retourner le type de page
		* Créée par Alexandre LG
		****/
		private function getType() {
			return plxMotor::getInstance()->mode;
		}
		
		/****
		* Récuperer le contenu
		* Créée par Alexandre LG
		****/
		private function getContent() {
			$plxMotor = plxMotor::getInstance();
			
			switch ($this->getType()) {
				case "article":
					$content = $plxMotor->plxRecord_arts->f('chapo').$plxMotor->plxRecord_arts->f('content');
				break;
				case "categorie":
					$content = plxUtils::getValue($plxMotor->aCats[$plxMotor->cible]['description']);
				break;
				case "static":
					$file = PLX_ROOT.$plxMotor->aConf['racine_statiques'].$plxMotor->cible;
					$file .= '.'.$plxMotor->aStats[$plxMotor->cible]['url'].'.php';
					$content = file_get_contents($file);
				break;
				default:
					$content = $plxMotor->aConf['title'];
			}
			
			return $content;
		}
		
		/****
		* Retourner le titre de la page
		* Créée par Alexandre LG
		****/
		private function getTitle() {
			$plxMotor = plxMotor::getInstance();
			
			switch ($this->getType()) {
				case "article":
					$title = $plxMotor->plxRecord_arts->f('title');
				break;
				case "static":
					$title = plxUtils::strCheck($plxMotor->aStats[$plxMotor->cible]['name']);
				break;	
				default:
					$title = $plxMotor->aConf['title'];
			}
			
			return $title;
		}
		
		/****
		* Retourner l'url courrante
		* Créée par Alexandre LG
		****/
		private function getUrl() {
			$url = filter_input(INPUT_SERVER, "REQUEST_URI");
			$url = ltrim($url, "/");
			
			return plxMotor::getInstance()->urlRewrite($url);
		}
		
		/****
		* Retourner la première image du contenu
		* Créée par Alexandre LG
		****/
		private function getImage() {
			$plxMotor = plxMotor::getInstance();
			
			// SI en mode article
			if ($this->getType() == "article") {
				// SI il n'y a pas d'image d'accorche
				if (empty($plxMotor->plxRecord_arts->f('thumbnail'))) {
					$content = $plxMotor->plxRecord_arts->f('chapo');
					$content .= $plxMotor->plxRecord_arts->f('content');
					if (preg_match('~<img[^>]*?src="(.*?)"[^>]+>~', $content, $match)) {
						$image = trim($match[1]);
					}
				}
				else {
					$image = $plxMotor->plxRecord_arts->f('thumbnail');
				}
			}
			else {
				$defaultImage = $plxMotor->aConf['racine_themes'].$plxMotor->style."/img/aplxSocialMetas.png";
				if (file_exists($defaultImage)) {
					$image = $defaultImage;
				}
			}
			
			if (!empty($image)) {
				$image = ltrim($image, "/");
				$image = plxMotor::getInstance()->urlRewrite($image);	
			}
			
			return $image;
		}
		
		/****
		* Retourner les deux premières phrases du contenu
		* Créée par Alexandre LG
		****/
		private function getDescription() {
			$plxMotor = plxMotor::getInstance();
			
			if (strip_tags($this->getContent()) == $plxMotor->aConf['title']) {
				$description = $plxMotor->aConf['description'];
			}
			else {
				$description = explode(PHP_EOL, $this->getContent())[0];
				$description = substr($description, 0, 180);
				$description = strip_tags($description);
			}
			
			$description = trim($description);
			$description = substr($description, 0, -3);
			$description = htmlentities($description);
			$description .= "...";
			return $description;
		}

		/****
		* Retourner le nom du site
		* Créée par Alexandre LG
		****/
		private function getSiteName() {
			return plxMotor::getInstance()->aConf['title'];
		}
		
		/****
		* Afficher les balises
		* Créée par Alexandre LG
		****/
		public function showMetas() {
			echo PHP_EOL;
			echo '
		<!-- Open Graph data -->
		<meta property="og:site_name" content="'.$this->getSiteName().'" />
		<meta property="og:title" content="'.$this->getTitle().'" />
		<meta property="og:type" content="'.$this->getType().'" />
		<meta property="og:url" content="'.$this->getUrl().'" />
		<meta property="og:description" content="'.$this->getDescription().'" />
		<meta property="og:image" content="'.$this->getImage().'" />
		<!-- Schema.org markup for Google+ -->
		<meta itemprop="name" content="'.$this->getTitle().'">
		<meta itemprop="description" content="'.$this->getDescription().'">
		<meta itemprop="image" content="'.$this->getImage().'" />
		<!-- Twitter Card data -->
		<meta name="twitter:card" content="summary">
		<meta name="twitter:site" content="'.$this->getSiteName().'">
		<meta name="twitter:title" content="'.$this->getTitle().'">
		<meta name="twitter:description" content="'.$this->getDescription().'">
		<meta name="twitter:image" content="'.$this->getImage().'" />
			';
			echo PHP_EOL;
		}
	}
?>
